#include "avg.h"
#include "unity.h"
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in integer_avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 1), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(2, integer_avg(2, 2), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_avg(1, 2), "Error in integer_avg 1.5");
	TEST_ASSERT_EQUAL_INT_MESSAGE(35, integer_avg(10, 60), "Error in integer_avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_avg(INT_MAX,INT_MAX ), "Error in integer_avg MAX");
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(INT_MIN,INT_MIN ), "Error in integer_avg MIN");

}

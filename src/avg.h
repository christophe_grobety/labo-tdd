#ifndef __AVG_H
#define __AVG_H

#include <stdint.h>

/** @return The AVG of two integers 32 @ref a and @ref b. */
int32_t integer_avg(int32_t a, int32_t b);

#endif //__AVG_H
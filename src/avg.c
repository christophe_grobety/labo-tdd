#include "avg.h"
#include <stdio.h>
#include <stdint.h>

int32_t integer_avg(int32_t a, int32_t b)
{
    return (a+b)/2;
}
